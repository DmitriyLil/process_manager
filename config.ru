# frozen_string_literal: true

require "./config/env"

dev = ENV["RACK_ENV"] == "development"

if dev
  require "logger"
  logger = Logger.new($stdout)
end

require "rack/unreloader"
Unreloader = Rack::Unreloader.new(subclasses: %w[Roda Sequel::Model],
                                  logger: logger,
                                  reload: dev) { ProcessManagerApp }

Unreloader.require("config/app.rb") { "ProcessManagerApp" }
run(dev ? Unreloader : ProcessManagerApp.freeze.app)
